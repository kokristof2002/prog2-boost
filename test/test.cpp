#ifdef CPORTA

#include "gtest_lite.h" // Magátől hozza a memtrace.h-t ha a MEMTRACE definiálva van így azt nem muszáj külön include-olni.

#include "complex.h"    // És az a biztos, ha a te header-jeid vannak legalul.

int main()
{
    TEST(Complex, getReal())
    {
        Complex Z = 1 + 1_i;
        EXPECT_DOUBLE_EQ(1.0, static_cast<double>(Z.getReal()));
    }
    END
    TEST(Complex, getImaginary())
    {
        Complex Z = 1 + 1_i;
        EXPECT_DOUBLE_EQ(1.0, static_cast<double>(Z.getImaginary()));
    }
    END
    TEST(Complex, getDistance())
    {
        Complex Z = 1 + 1_i;
        EXPECT_DOUBLE_EQ(std::sqrt(2), static_cast<double>(Z.getDistance()));
    }
    END
    TEST(Complex, getAngle())
    {
        Complex Z = 1 + 1_i;
        EXPECT_DOUBLE_EQ(M_PI / 4, static_cast<double>(Z.getAngle()));
    }
    END
    return 0;
}

#endif // CPORTA