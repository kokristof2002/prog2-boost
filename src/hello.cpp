#include <iostream>   // Előre kerül szinte az összes standard fejlécállomány.

#include "memtrace.h" // A memtrace-nek utánuk kell jönnie, mert különben eltörhet.

#include "hello.h"    // És az a biztos, ha a te header-jeid vannak legalul.

std::size_t Hello::greet(const std::string &name)
{
    std::cout << "Hello " << name << '!' << std::endl;
    return name.length();
}
