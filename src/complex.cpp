#include <cmath>      // Előre kerül szinte az összes standard fejlécállomány.

#include "memtrace.h" // A memtrace-nek utánuk kell jönnie, mert különben eltörhet.

#include "complex.h"  // És az a biztos, ha a te header-jeid vannak legalul.

Complex::Complex(long double real, long double imaginary)
    : _real{real}, _imaginary{imaginary} {}

long double Complex::getReal() const
{
    return _real;
}

long double Complex::getImaginary() const
{
    return _imaginary;
}

long double Complex::getDistance() const
{
    return std::sqrt(_real * _real + _imaginary * _imaginary);
}

long double Complex::getAngle() const
{
    return std::atan2(_imaginary, _real);
}

Complex operator"" _i(long double imaginary)
{
    return Complex{0.0, imaginary};
}

Complex operator"" _i(unsigned long long imaginary)
{
    return Complex{0.0, static_cast<long double>(imaginary)};
}

Complex operator+(long double x, const Complex &Z)
{
    return Complex{x} + Z;
}

Complex operator+(const Complex &Z, long double x)
{
    return Z + Complex{x};
}

Complex operator+(const Complex &Z1, const Complex &Z2)
{
    long double real = Z1.getReal() + Z2.getReal();
    long double imaginary = Z1.getImaginary() + Z2.getImaginary();
    return Complex{real, imaginary};
}

Complex operator-(long double x, const Complex &Z)
{
    return Complex{x} - Z;
}

Complex operator-(const Complex &Z, long double x)
{
    return Z + Complex{x};
}

Complex operator-(const Complex &Z1, const Complex &Z2)
{
    long double real = Z1.getReal() - Z2.getReal();
    long double imaginary = Z1.getImaginary() - Z2.getImaginary();
    return Complex{real, imaginary};
}

Complex operator*(long double x, const Complex &Z)
{
    return Complex{x} * Z;
}

Complex operator*(const Complex &Z, long double x)
{
    return Z * Complex{x};
}

Complex operator*(const Complex &Z1, const Complex &Z2)
{
    long double x = Z1.getReal();
    long double y = Z1.getImaginary();
    long double u = Z2.getReal();
    long double v = Z2.getImaginary();
    long double real = x * u - y * v;
    long double imaginary = x * v + y * u;
    return Complex{real, imaginary};
}

Complex operator/(long double x, const Complex &Z)
{
    return Complex{x} / Z;
}

Complex operator/(const Complex &Z, long double x)
{
    return Z / Complex{x};
}

Complex operator/(const Complex &Z1, const Complex &Z2)
{
    long double a = Z1.getReal();
    long double b = Z1.getImaginary();
    long double c = Z2.getReal();
    long double d = Z2.getImaginary();
    long double real = (a * c + b * d) / (c * c + d * d);
    long double imaginary = (b * c - a * d) / (c * c + d * d);
    return Complex{real, imaginary};
}
