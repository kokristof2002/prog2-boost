#ifndef CPORTA

                      // Ide jöhene az összes standard fejlécállomány.

#include "memtrace.h" // A memtrace-nek utánuk kell jönnie, mert különben eltörhet.

#include "hello.h"    // És az a biztos, ha a te header-jeid vannak legalul.

int main()
{
    std::size_t length = Hello::greet("Prog2 labor");
    return length; // Erre elvileg kell, hogy szóljon a SonarLint!
}

#endif // CPORTA