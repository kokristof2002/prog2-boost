# Nagyházi kiinduló projekt

## Mi ez és miért?

A C++-ban való fejlesztés elsőre elég nehézkes tud lenni. A Code::Blocks egy működő megoldás, de azért bőven van vele szívás. Ez a projekt egy szerintem jobban használható alternatívát kíván nyújtani.

Több okból is ajánlom:
- A Visual Studio Code-nak nagyon jó a debugger támogatása, ugyanazokat a dolgokat sokkal érthetőbben jelenítí meg, ezzel a hatékonyságot növelve.
- Bekonfiguráltam a Doxygen-t is. Ha szépen kommentezed a header fájljaidat, akkor mindent le fog tudni neked generálni és majd nem kell a dokumentáció megírásával szívnod.
- A kód fordításához egy CMake nevű program van használva. Ez azért nagyon jó, mert így parancssorból is könnyen fordítható a kódotok, nem vagytok hozzáláncolva egyik fejlesztői környezethez sem, akár egy egyszerű szövegszerkesztőből is írhatnád a házit, a build.sh script futtatásával teljesen ugyanúgy fordulna.
- Működik a Szebi féle teszelés. Csak egy kommentet kell hozzá kiszedni és máris a tesztek fognak futni a rendes kód helyett.
- A SonarLint is kell, hogy működjön alapból. Ez nagyon sok potenciális hibát meg tud találni anélkül, hogy fordítanád a kódot. Még később lesz róla szó.

## Hogyan töltsem le?

### Ha profi vagy

Akkor csak klónozd le a repót: `git clone git@gitlab.com:Melidon/prog2-boost.git`.
### Ha még nem használtál git-et

Akkor itt az ideje, hogy elkezdjél! De egyelőre az is jó lesz, ha csak [ZIP](https://gitlab.com/Melidon/prog2-boost/-/archive/master/prog2-boost-master.zip)-ként töltöd le a projektet.

## Mire van szükségem ahhoz, hogy működjön?

- Mint minden C++ projekt fordításához, ehhez is kell egy fordító például GCC. Jó eséllyel már van is a gépeden. Még később lesz az elérési útvonala, úgyhogy nem árt ha utánanézel, hogy hova van pontosan feltelepítve.
- Szükséged lesz még a Make-re és CMake-re is (ez két külön program!). Ha Linux-ról vagy, akkor már jó eséllyel van a gépeden. Windows esetében ez nem valószínű, de elvileg simán le kell tudnod tölteni őket. Ehhez [itt](https://aneescraftsmanship.com/how-to-install-and-configure-cmake-in-windows/) egy kis segítség.
- Kell még Visual Studio Code is. Jó cucc, nem fogod megbánni.

## Hogyan fogom tudni működésre bírni?

Úgy állítottam be, hogy az első indításkor a Visual Studio Code megkérdezze, hogy szeretnéd-e feltelepíteni az ajánlott kiegészítéseket. Erre lehetőleg mondj igent, sokat fognak segíteni a fejlesztésben.

Linux esetében már kész is vagy. Nyomj egy F5-öt a fordításhoz és futtatáshoz!

Windows esetében még mindenképpen át kell állítani pár dolgot a tökéletes működés érdekében.
- A .vscode mappában találsz egy launch.json fájlt. A **"program"** kezdetű (11.) sorban a NHF végére oda kell írnod, hogy **.exe**. Itt adjuk meg, hogy mi lesz a program neve amit majd futtatni kell.
- Szintén a launch.json-ben. A **"miDebuggerPath"** kezdetű (31.) sorban meg kell adnod, hogy hova van telepítve neked a gdb. Ha például korábban Cod::Blocks-ot használtál MinGW-vel, akkor valami ilyesmi kell: **"C:\Program Files\CodeBlocks\MinGW\bin\gdb.exe"**.
- A .vscode mappában van még egy tasks.json fájl is. Ez mondja meg, hogy hogyan kell lefordítani a projektet. Jelen esetben ezt nem itt végezzük, a hívást csak továbbpasszoljuk a fordítást végző scriptnek, úgyhogy a A **"command"** kezdetű (7.) sorban a build script kiterjesztését át kell, hogy állítsad **.bat**-ra.
- Menj biztosra, hogy a cmake és a gcc is elérhető a path-ban. Ezek teszteléséhez nyiss egy konzolt és írjad be, hogy: `cmake --version`, illetve: `gcc --version`. Ha nem dobtak hibát akkor jó vagy.
- A build.bat-ban meg van adva a gcc elérési útvonala, Ez akkor kell, ha a gcc nem elérhető a path-ban, úgyhogy szükség szerint frissítsed! Elvileg kell, hogy menjen enélkül is, úgyhogy igazából töröld ki bátran. De csak ezt az egyet, a többi még kell!

Ha még most sem megy, akkor nagyon bocsi, de nem tudok segíteni, már nagyon rég nem használtam Windows-t.

## Hogyan tudok új forrásfájlokat hozzáadni?

A header fájlokat pakoljad be az include mappába. A cpp forrásfájlok pedig kerüljenek az src mappába. Pont úgy kell tenned, ahogy a hello.h és hello.cpp-nél van a példában.

Még egészítsed ki a CMakeLists.txt-ben a forrásfájlos részt. Csak pakold be az új forrásfájlokat a hello.cpp alá ahhoz hasonlóan. Fontos, hogy a headerek ide nem kellenek!

## Hogyan tudom használni a Doxygen-t?

Először is szükséged lesz Doxygen-re. Ezt töltsed le valahonnan! Másodszor is ellenőrizd le, hogy benne van-e a path-ban. Ha már ott van, akkor nyiss egy új terminált (bal felül Terminal -> New Terminal) és írjad be, hogy `doxygen Doxyfile`. Ez mindent legenerál a generated-docs nevű mappába. Ezen belül lesz még egy html mappa, amin beül az index.html-t megnyitva megtekintheted a generált dokumentációt. Ha valami nem tetszene, akkor nyugodtan állíts át pár beállítást a Doxyfile-ban.

# Tesztelés

Először is fontos, hogy olyan kód legyen írva, amit jól lehet tesztelni. Mi kell ehhez pontosan?
- Jól kell használni a memtrace.h-t.
  - Minden fordítási egységében biztosítani kell, hogy a memtrace.h állomány feldolgozásra kerüljön a standard fejlécállományok után!
  - Viszon amennyiben a saját header-ödben van memrória foglalás vagy felszabadítás, akkor annak mindenképpen utána kell lennie.
  - Az a legegyszerűbb, ha az összes saját utána van, így tuti, hogy nem fogod elrontani.
- Kettő main függvényt kell írnod.
  - Ezeket célszerű külön fájlba is rakni.
  - Az egyik csak akkor legyen benne a kódba, ha a CPORTA definiálva van, a másik meg pont akkor ha nincsen.
  - Amelyiknél a tesztek vannak, oda kell a gtest_lite.h is. Ez csak ide kell, viszont ő hozza magával a memtrace.h-t is, úgyhogy ez az egyetlen kivégel, hogy az nem kell, de pont ezért utolsónak kell lennie.

Ha valami nem lenne ezzel kapcsolatban tiszta, akkor mindenképpen nézzél bele a kódba! Mindenre van példa és igyekeztem azokat is kommentekkel ellátni.

A Szebi féle tesztek futtatásához szükség van fodításkor a `-DCPORTA` és `-DMEMTRACE` kapcsolókra. Ez a CMakeLists.txt-ben már ott vannak, csak ki vannak kommentezve. Ha kiszeded a kommentet, akkor a tesztek fognak futni.

## Mi az a SonarLint?

A SonarLint statikus kódanalizálást csinál. Nagyon sok mindenben tud segíteni, sok apróságra felhívja az ember figylemét. Az egyetlen talán az lehet, hogy valószínűleg olyan dolgokat is fog ajánlani amik nincsenek tanítva prog2-ből. Úgyhogy kérlek ésszel használd, tehát ha felajánl valamit, hogy az úgy jobb lenne, akkor csak abban az esetben használjad ha érted is! Ha feltelepítetted a projekthez ajánlott extension-öket (amik tartalmazzák a SonarLint-et is), akkor amint rámész egy forrásfájlra, monjuk a main.cpp, el fog indulni a SonarLint és fel fog dobni egy értesítést, hogy _SonarLint is unable to analyze C and C++ file(s) because there is no configured compilation database._ Ha úgy döntöttél, hogy szeretnéd használni, akkor erre majd azt kell mondanod, hogy persze, **Configure compile commands**. Fontos, hogy ez csak akkor fog működni ha már előtte legalább egyszer futtattad a projektet (fordításkot a CMake generál a build mappába egy _compile_commands.json_-t, a SonarLint-nek szüksége van erre a működéshez). Ha mindent jól csináltál, akkor a main.cpp-ben kell, hogy kapj egy aláhúzást, mert a return-nél nem egyeznek meg a típusok.
