#pragma once

#include <string>

/**
 * @brief This is the brief descripiton of the Hello class.
 *
 * You can add your detailed descripiton here.
 */
class Hello
{
public:
    /**
     * @brief Greets a person.
     *
     * Prints a greeting for the specified person and returns the length of his/her name.
     *
     * @param name The name of the person to be greeted.
     *
     * @return std::size_t The length of the given name.
     */
    static std::size_t greet(const std::string &name);
};
