#pragma once

/**
 * @brief This class represents a complex number.
 *
 * @details Every complex number can be expressed in the form z = a + bi, where a and b are real numbers. An alternative
 * option for coordinates in the complex plane is the polar coordinate system that uses the distance of the point z
 * from the origin (O), and the angle subtended between the positive real axis and the line segment Oz in a
 * counterclockwise sense.
 */
class Complex final
{
private:
    long double _real;
    long double _imaginary;

public:
    /**
     * @brief Construct a new Complex object.
     *
     * @param real This parameter will be the real part of the complex number. It is 0 by default.
     * @param imaginary This parameter willl be the imaginary part of complex number. It is 0 by default.
     */
    Complex(long double real = 0.0, long double imaginary = 0.0);

    /**
     * @brief Getter for the real part of the complex number.
     *
     * @return long double The real part.
     */
    long double getReal() const;

    /**
     * @brief Getter for the imaginary part of the complex number.
     * 
     * @return long double The imaginary part.
     */
    long double getImaginary() const;

    /**
     * @brief Getter for the distance of the point z from the origin (O).
     * 
     * @return long double The distance.
     */
    long double getDistance() const;

    /**
     * @brief Getter for the angle subtended between the positive real axis and the line segment Oz in a counterclockwise sense.
     * 
     * @return long double The angle.
     */
    long double getAngle() const;
};

// Igen, mindet meg lehetne írni inline ami ebben a header-ben van, csak a példa kedvéért vannak megírva a függvények törzse külön.
// És igen a nagyháziban az operátorokhoz is illik rendes kommentet írni.

Complex operator"" _i(long double imaginary);
Complex operator"" _i(unsigned long long imaginary);

Complex operator+(long double x, const Complex &Z);
Complex operator+(const Complex &Z, long double x);
Complex operator+(const Complex &Z1, const Complex &Z2);

Complex operator-(long double x, const Complex &Z);
Complex operator-(const Complex &Z, long double x);
Complex operator-(const Complex &Z1, const Complex &Z2);

Complex operator*(long double x, const Complex &Z);
Complex operator*(const Complex &Z, long double x);
Complex operator*(const Complex &Z1, const Complex &Z2);

Complex operator/(long double x, const Complex &Z);
Complex operator/(const Complex &Z, long double x);
Complex operator/(const Complex &Z1, const Complex &Z2);
